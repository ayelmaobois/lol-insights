require('dotenv').config();

function envWithDefault(name, defaultValue = null) {
  const envVar = process.env[name];
  if (envVar === undefined) {
    console.log(`Environment variable [${name}] not found, defaulting to [${defaultValue}]`);
    return defaultValue;
  }
  return envVar;
}

const httpConfig = {
  baseURL: envWithDefault('RIOT_API_URL', 'https://euw1.api.riotgames.com'),
  timeout: envWithDefault('RIOT_API_TIMEOUT', 5000),
  headers: {
    'X-Riot-Token': envWithDefault('RIOT_API_TOKEN'),
  },
};

// Exported Config
const globalConfig = {
  http: {
    port: envWithDefault('API_PORT', 8080),
  },
};

function buildAxiosConfig(path) {
  const exportedConfig = { ...httpConfig };
  exportedConfig.baseURL += path;
  return exportedConfig;
}

module.exports = {
  buildAxiosConfig,
  globalConfig,
};
