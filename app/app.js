const express = require('express');

const config = require('./config').globalConfig;
const match = require('./match');
const summoner = require('./summoner');

const app = express();

// Operational checks
app.get('/ready', (_, res) => {
  res.send({ status: 'OK' });
});

// API routing
app.get('/summoner/:summonerName', async (req, res) => {
  const { summonerName } = req.params;
  const summonerData = await summoner.getSummoner(summonerName);
  res.send(summonerData);
});

app.get('/summoner/:summonerName/matches', async (req, res) => {
  const { summonerName } = req.params;
  const matchData = await match.getMatchDataBySummonerName(summonerName);
  res.send(matchData);
});

// Run service on API_PORT
app.listen(config.http.port, () => {
  console.log(`Listening on port: ${config.http.port}`);
});
