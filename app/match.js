const axios = require('axios');
const config = require('./config');
const summoner = require('./summoner');

// const matchEndpoint = axios.create(config.buildAxiosConfig('/lol/match/v4/matches/'));
const matchesEndpoint = axios.create(config.buildAxiosConfig('/lol/match/v4/matchlists/by-account/'));

async function getMatchDataBySummonerId(summonerId) {
  // TODO - handle pagination
  const res = await matchesEndpoint.get(summonerId);
  console.log(`Retrieved match data for summonerId [${summonerId}]: ${JSON.stringify(res.data.matches)}`);
  return res.data.matches.map((match) => ({
    matchId: match.gameId,
    championId: match.champion,
  }));
}

async function getMatchDataBySummonerName(summonerName) {
  const summonerId = await summoner.getSummonerId(summonerName);
  return getMatchDataBySummonerId(encodeURI(summonerId));
}

module.exports = {
  getMatchDataBySummonerName,
};
