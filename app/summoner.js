const axios = require('axios');
const config = require('./config');

const summonerEndpoint = axios.create(config.buildAxiosConfig('/lol/summoner/v4/summoners/by-name/'));

async function getSummoner(summonerName) {
  const res = await summonerEndpoint.get(encodeURI(summonerName));
  console.log(`Retrieved summoner data for summonerName [${summonerName}]: ${JSON.stringify(res.data)}`);
  return res.data;
}

async function getSummonerId(summonerName) {
  const summoner = await getSummoner(summonerName);
  return summoner.accountId;
}

module.exports = {
  getSummoner,
  getSummonerId,
};
